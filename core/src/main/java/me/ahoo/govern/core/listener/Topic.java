package me.ahoo.govern.core.listener;

/**
 * @author ahoo wang
 */
public interface Topic {
    String getTopic();
}
