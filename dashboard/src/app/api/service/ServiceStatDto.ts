export interface ServiceStatDto {
  serviceId: string;
  instanceCount: number;
}
