export interface StatDto {
  namespaces: number;
  services: number;
  instances: number;
  configs: number;
}
